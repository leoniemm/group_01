methods
=======

.. toctree::
   :maxdepth: 4

   energy_mix
   method_01
   method_02
   method_03
   method_04
   method_05
   method_06
   method_07_extra
   method_08
   method_09
   method_10
