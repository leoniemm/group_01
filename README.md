# Group_01

Project Ergo is part of the 2612-Advanced Programming for Data Science Course at NOVA SBE. 
Group_01 consits of 4 motivated Business Analytics Students and the Scenrio for the challenges was a two-day hackathon promoted by a company to study the energy mix of several countries. By analysing the energy mix of several countries, the company ultimately seeks to contribute to the green transition by having a more savvy taskforce.

## Initialization
To start the project you need just go to the "showcase_notebook.ipynb". 
There just run all the cells and follow our little Data Journey. 

Data Source: https://github.com/owid/energy-data?country= 

### Technologies
This Project is based on Python 3.9 and uses several common libraries, which need to be installed first.


## Authors 
- Alienor Bosse 49675@novasbe.pt
- Constantin Baumann 49620@novasbe.pt
- Leonie Mostbeck 49238@noasbe.pt
- Tara Tumbrägel 48333@novasbe.pt

## License
This Project is equipped with a MIT License. Please refer to this file for further details.

