"""
This contains a method that should plot a scatter plot where x is gdp, y is
total energy consumption, and the area of each dot is population.
"""

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import plotly.express as px

pd.plotting.register_matplotlib_converters()


def gapminder(self, year):
    """
    This method should receive an argument year (user's choice) of type int.
    If the received argument is not an int, the method should raise a TypeError.
    This method should plot a scatter plot where x is gdp, y is total energy consumption,
    and the area of each dot is population.

    Parameters
    ------------
    userinput: int
        Default user input.

    input_energy_df: dataframe
        all plots are based on this dataframe which is filtered.

    Returns
    ---------
    Returns a scatterplot similar in design to the gapminder plot of
    Hans Rosling.

    Example
    ---------
    https://www.gapminder.org/tools/#$chart-type=bubbles&url=v1
    """

    # default user input
    userinput = year

    if type(userinput) not in [int]:
        raise TypeError("Chosen year is not int.")
    # clean dataframe
    user_df = self.energy_df[self.energy_df.year_int == userinput].copy()
    input_energy_df_first = user_df.dropna(subset=["gdp", "Total Consumption"])
    input_energy_df = input_energy_df_first[
        input_energy_df_first["country"] != "World"
    ].copy()
    input_energy_df["population"].fillna(0, inplace=True)

    # plot
    fig = px.scatter(
        input_energy_df,
        x="gdp",
        y="Total Consumption",
        color="country",
        size="population",
        hover_name="country",
        log_x=True,
        log_y=True,
        size_max=60,
        labels={
            "gdp": "GDP in USD",
            "Total Consumption": "Energy Consumption in Terrawat",
        },
        title="Worldwide Trend in GDP, Energy Consumption and Population per Country",
    )
    return fig.show()
