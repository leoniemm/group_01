"""
This contains a method which may receive a string with a country or a list of country strings.
This method can compare the total of the "_consumption" columns for each of the chosen countries
and plot it so a comparison can be made (considering only the years after 1970 if applicable).
"""

import numpy as np
import seaborn as sns
import plotly.express as px

def plot_totalconsumption(self, countries):
        """
        The outcome is a plot of a line chart of the different consumptions for the chosen country.
        Before that we normalize the consumptions so they add up to 100% in a year.

        Parameters
        ---------------
        new_energy_df: dataframe
            The original dataframe reduced to the consumption, year and country
            with an additional column "Total Consumption".
            The consumption columns will then be normalized
        df_plot: dataframe
            New dataframe with the received string or list of country strings.

        Returns
        ---------------
        plot_totalconsumption: plot
            The line plot shows the total consumptions over time for the chosen country

        """
        #Personalize the dataframe with the necessary "_consumption" columns to compare the total
        new_energy_df = self.energy_df.loc[:, self.energy_df.columns.str.endswith('consumption')].copy()
        new_energy_df["Total Consumption"] = new_energy_df.apply(np.sum, axis=1)
        new_energy_df["year"] = self.energy_df["year"] #add year to new datadrame
        new_energy_df["country"]= self.energy_df["country"] #add country to new dataframe

        #Plotting the string or list of country strings after checking for the type
        if isinstance(countries,str):
            df_plot= new_energy_df[new_energy_df["country"]== countries]
        else:
            df_plot = new_energy_df[new_energy_df["country"].isin(countries)]
        fig = px.line(df_plot, x="year", y="Total Consumption", color= "country", title='Total Energy Consumption per Country over the years',
                hover_name="country",
                         labels={
                     "year": "Year",
                     "Total Consumption": "Total Energy Consumption in Terrawat"
                 })
        return fig.show()
