"""
This contains a method that downloads a file and creates a dataframe out of it
(considering only the years after 1970 if applicable).
"""


from urllib.request import urlretrieve
import os  # we want python to be able to read what we have in our hard drive
import pandas as pd



def download_file(self, save_path: str = "./downloads",
                      output_file: str = "energydata.csv") -> pd.core.frame.DataFrame: 
        """
        Downloads a file from an URL into a new folder called 'downloads' your hard drive
        and reads it into a panda dataframe.

        Parameters
        ------------
        file_link: str
            A string containing the link to the file you wish to download.
        save_path: str
            A string containing the name of the path to where the folder shall be saved.
        output_file: str
            A string containing the name of the output file. The default value is 'file.csv'
            at the location you are running the function.

        Returns
        ---------
        The Output is the csv read into a pandas dataframe.
        This will only contain the data from 1970 onwards of the Example File.


        Example
        ---------
        download_file("https://nyc3.digitaloceanspaces.com/owid-public/data" \
        "/energy/owid-energy-data.csv")
        """
        #create the path where you want the file to be saved
        path = os.path.join(save_path)
        download_path = path + "/" + output_file

        #save your current directory so you can get back to working right where you left of
        current_dir = os.getcwd()

        #use our url
        file_link = self.url

        #If file doesn't exist, create a folder and download it there.
        #Else, print a warning message.
        if not os.path.exists(download_path):
            if os.path.exists(path):#if someone already has a downloads folder, just retrive the file 
                os.chdir(path) #make it your current working directory
                urlretrieve(file_link, filename=output_file) #retrive the file
                os.chdir(current_dir) #change back to previous wd
            else: #download folder doesn´t exists, need to be created and file retrived there 
                os.makedirs(path) #create the path
                os.chdir(path) #make it your current working directory
                urlretrieve(file_link, filename=output_file) #retrive the file
                os.chdir(current_dir) #change back to previous wd
        else:
            print("File already exists!")
  


        #Once the file is in the folder, it will be read into a dataframe
        energy_df = pd.read_csv(r"" + save_path + "/" +  output_file)

        #we only want to consider years from 1970 onwards
        energy_df = energy_df[(energy_df.year >= 1970) & (energy_df.year <= 2015)]
        #make one year column in integer format 
        energy_df["year_int"] = energy_df["year"]
        #make the year column datetime format
        energy_df["year"] = pd.to_datetime(energy_df.year.astype(str), format = "%Y%")
        #make it the index
        energy_df.set_index("year")
        
        #assign it to our class
        self.energy_df = energy_df
        