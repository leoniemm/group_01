def enrich_df(self) -> None:
    """
    Enriches the attribute "energy_df" with the column "emissions", which are the
    emissions per energy source in tonnes of CO2.
    """

    # create a dictionary with the emission multiples per energy source
    emission_multis = {
        "biofuel": 1450,
        "coal": 1000,
        "gas": 455,
        "hydro": 90,
        "nuclear": 5.5,
        "oil": 1200,
        "solar": 53,
        "wind": 14,
    }

    def calc_emissions(df_row: pd.Series) -> float:
        """
        This method calculates the total emissions for a certain row of the dataframe

        Parameters
        ---------------
        df_row: pd.Series 
            The row for which the emissions should be calculated

        Returns
        ---------------
        sum_emissions: float
            The total sum of emissions per row of the dataframe.
        """

        # create an empty list in which the emissions per energy type are saved
        emissions = []
        # run through the dictonary, find the corresponding value and do the computation
        for e_name, e_mult in emission_multis.items():
            # get the corresponding consumption value of the row of the dataset
            part_consumption = df_row[e_name + "_consumption"]

            # calculates the total emissions and coverts the number into tonnes CO2
            emission_source = (e_mult / 1e6) * (1e9 * part_consumption)

            # append the calculation to the emissions list
            emissions.append(emission_source)

        # sum up the individual values to a total sum of emissions per energy source and
        # return the value
        sum_emissions = sum(emissions)
        return sum_emissions

    self.energy_df["emissions"] = self.energy_df.apply(calc_emissions, axis=1)
