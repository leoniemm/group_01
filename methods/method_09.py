def emission_scatter(self, year):
    """
    Creates a scatter plot between emissions and consumption for all countries.
    The size of the dots is the population. The user has to determine a year.

    Parameter
    ---------------
    year: int
        The year for which the scatter plot should be created.

    Returns
    ---------------
    my_output: plot
        The scatter plot shows the energy consumption and the corresponding emissions
        of multiple countries for a particular year.
    """

    # input from the user - if input is not an integer, raise a TypeError
    functioninput = year

    if type(functioninput) not in [int]:
        raise TypeError("Chosen year is not int.")

    # clean dataset
    filter1_df = self.energy_df[self.energy_df["year_"] == functioninput].copy()
    filter2_df = filter1_df[filter1_df["country"] != "World"]

    # plot the data
    fig = px.scatter(
        filter2_df,
        x="emissions",
        y="total_consumption",
        color="country",
        size="population",
        hover_name="country",
        log_x=True,
        log_y=True,
        size_max=60,
        labels={
            "emissions": "Emissions in tC02eq/tWhSD",
            "total_consumption": "Energy Consumption in Terawat",
        },
        title="Worldwide Trend in Emissions, Energy Consumption, and Population per Country in "
        + str(year),
    )
    return fig.show()
