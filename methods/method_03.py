"""
This is method 3 which plots the different consumption types for the chosen country
"""

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

def plot_area(self, country_chosen):
        """
        The outcome is a plot of an area chart of the different consumptions for the chosen country.
        Before that we normalize the consumptions so they add up to 100% in a year.

        Parameters
        ---------------
        consumption_df: dataframe
            The original dataframe reduced to the consumption, year and country columns.
            The consumption columns will then be normalized
        cols_to_norm: list
            The list including all the consumption column names

        Returns
        ---------------
        my_output: plot
            The area plot shows the different consumptions over time for the chosen country
        """

        consumption_df = self.energy_df.loc[:, self.energy_df.columns.str.endswith("consumption")]

        # Normalize values
        consumption_df = consumption_df.div(consumption_df.sum(numeric_only=True, axis=1), axis=0)
        # Add country and year
        consumption_df["year"] = self.energy_df["year"]
        consumption_df["country"] = self.energy_df["country"]

        # Chosen country
        consumption_df = consumption_df[consumption_df["country"]==country_chosen].copy()
        if consumption_df.empty:
            raise ValueError("This country is not in the dataframe")

        # Create area chart
        sns.set(rc={'figure.figsize':(20,12)})
        sns.set_theme()

        plt.stackplot(consumption_df.year, consumption_df.biofuel_consumption,
                      consumption_df.coal_consumption, consumption_df.fossil_fuel_consumption,
                      consumption_df.gas_consumption, consumption_df.hydro_consumption,
                      consumption_df.low_carbon_consumption, consumption_df.nuclear_consumption,
                      consumption_df.oil_consumption, consumption_df.other_renewable_consumption,
                      consumption_df.primary_energy_consumption, consumption_df.renewables_consumption,
                      consumption_df.solar_consumption, consumption_df.wind_consumption,
                      labels=["biofuel_consumption", "coal_consumption",
                              "fossil_fuel_consumption", "gas_consumption",
                              "hydro_consumption", "low_carbon_consumption",
                              "nuclear_consumption", "oil_consumption",
                              "other_renewable_consumption", "primary_energy_consumption",
                              "renewables_consumption", "solar_consumption",
                              "wind_consumption"])

        # Add legend
        plt.legend(loc='upper left')

        # Add axis labels
        plt.xlabel('Years')
        plt.ylabel('Consumptions')

        # Display area chart
        plt.show()
        