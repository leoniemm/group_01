"""
This is an extra method which shows the population development over the years.
"""
import plotly.express as px
import pandas as pd
import numpy as np
import seaborn as sns

def population_dev(self, countries):
        """
        The outcome is a plot of a line chart of the population for the chosen country.
        This method receives a string or list with the countries.
        It compares the population of each country over the years.

        Parameters
        ---------------
        chosen_df: dataframe
            The country_gdp_df reduced to the country chosen

        Returns
        ---------------
        my_output: lineplot
            The plot shows the evolution of the gdp over time
        """
        
        if isinstance(countries, str):
            chosen_df = self.energy_df[self.energy_df["country"] == countries].copy()
        else:
            chosen_df = self.energy_df[self.energy_df["country"].isin(countries)].copy()
        fig = px.line(chosen_df, x="year", y="population", color= "country", title='Population development over the years',
                hover_name="country",
                         labels={
                     "year": "Year",
                     "population": "Population"
                 })
        return fig.show()
        