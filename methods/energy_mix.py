"""
This contains a class that with several methods useful for data analysis of
Energy Consumption across the globe.
"""

from urllib.request import urlretrieve
import os
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import plotly
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from statsmodels.tsa.arima.model import ARIMA
import warnings
warnings.filterwarnings('ignore')

pd.plotting.register_matplotlib_converters()


class EnergyMix:
    """
    This is a class to analyize the energy consumption and mix globally
    since 1970. Takes into consideration also GDP, and Population.
    """

    def __init__(self):
        self.energy_df = pd.DataFrame()
        self.url = "https://nyc3.digitaloceanspaces.com/owid-public/data/energy/owid-energy-data.csv"
        self.consumption_cols = [
            "biofuel_consumption",
            "coal_consumption",
            "gas_consumption",
            "hydro_consumption",
            "nuclear_consumption",
            "oil_consumption",
            "other_renewable_consumption",
            "solar_consumption",
            "wind_consumption",
        ]

    def download_file(
        self, save_path: str = "./downloads", output_file: str = "energydata.csv"
    ) -> pd.core.frame.DataFrame:
        """
        Downloads a file from an URL into a new folder called 'downloads' your hard drive
        and reads it into a panda dataframe.

        Parameters
        ------------
        file_link: str
            A string containing the link to the file you wish to download.
        save_path: str
            A string containing the name of the path to where the folder shall be saved.
        output_file: str
            A string containing the name of the output file. The default value is 'file.csv'
            at the location you are running the function.

        Returns
        ---------
        The Output is the csv read into a pandas dataframe.
        This will only contain the data from 1970 onwards of the Example File.

        Example
        ---------
        download_file("https://nyc3.digitaloceanspaces.com/owid-public/data" \
        "/energy/owid-energy-data.csv")
        """
        # create the path where you want the file to be saved
        path = os.path.join(save_path)
        download_path = path + "/" + output_file

        # save your current directory so you can get back to working right where you left of
        current_dir = os.getcwd()

        # use our url
        file_link = self.url

        # If file doesn't exist, create a folder and download it there.
        # Else, print a warning message.
        if not os.path.exists(download_path):
            if os.path.exists(
                path
            ):  # if someone already has a downloads folder, just retrive the file
                os.chdir(path)  # make it your current working directory
                urlretrieve(file_link, filename=output_file)  # retrive the file
                os.chdir(current_dir)  # change back to previous wd
            else:  # download folder doesn´t exists, need to be created and file retrived there
                os.makedirs(path)  # create the path
                os.chdir(path)  # make it your current working directory
                urlretrieve(file_link, filename=output_file)  # retrive the file
                os.chdir(current_dir)  # change back to previous wd
        else:
            print("File already exists!")

        # Once the file is in the folder, it will be read into a dataframe
        energy_df = pd.read_csv(r"" + save_path + "/" + output_file)

        # we only want to consider years from 1970 onwards
        energy_df = energy_df[(energy_df.year >= 1970) & (energy_df.year <= 2015)]
        #make one year column in integer format
        energy_df["year_int"] = energy_df["year"]
        # make the year column  datetime format
        energy_df["year"] = pd.to_datetime(energy_df.year.astype(str), format="%Y%")
        # make it the index
        energy_df = energy_df.set_index("year")

        # assign it to our class
        self.energy_df = energy_df

    def unique_column_values(self) -> list:
        """
        This function outputs a list of the unique countries in the dataframe

        Parameters
        ---------------
        unique_values: list
        list of the unique countries in the dataframe

        Returns
        ---------------
        self.unique_values: list
        list of the unique countries in the dataframe
        """
        unique_values = list(self.energy_df["country"].unique())
        self.unique_values = unique_values
        return self.unique_values

    def plot_area(self, country_chosen: str, normalize: bool = True) -> None:
        """
        The user has the possibility to input a country and to choose normalization or not.
        If the received country is not in the unique_column_values() list the method should raise a TypeError.
        The outcome is a plot of an area chart of the different consumptions for the chosen
        country. We take as a default, that we normalize the consumptions so they add
        up to 100% in a year. If the user does not want that, he can use the normalize argument and
        pass it another value then True.

        Parameters
        ---------------
        countries_list: list
            calls the list from unique column values
        consumption_df: dataframe
            The original dataframe reduced to the consumption, year and country columns.
            The consumption columns will then be normalized
        cols_to_norm: list
            The list including all the consumption column names

        Returns
        ---------------
        fig.show(): None
            The area plot shows the different consumptions over time for the chosen country
        """
        countries_list = self.unique_column_values()

        if country_chosen not in countries_list:
            raise ValueError("This country is not in the dataframe")

        consumption_df = self.energy_df[self.consumption_cols].copy()
        # Add country and year
        consumption_df["year"] = self.energy_df.index
        consumption_df["country"] = self.energy_df["country"]
        # Chosen country
        consumption_df = consumption_df[
            consumption_df["country"] == country_chosen
        ].copy()

        if normalize:
            # apply normalization techniques on Column 1
            columns = self.consumption_cols
            consumption_df[columns] = (
                consumption_df[columns] / consumption_df[columns].abs().max()
            )

        fig = go.Figure()
        fig.add_trace(
            go.Scatter(
                x=consumption_df["year"],
                y=consumption_df["biofuel_consumption"],
                name="Biofuel Consumption",
                mode="lines",
                line=dict(width=0.5, color="orange"),
                stackgroup="one",
                groupnorm="percent",
            )
        )
        fig.add_trace(
            go.Scatter(
                x=consumption_df["year"],
                y=consumption_df["coal_consumption"],
                name="Coal Consumption",
                mode="lines",
                line=dict(width=0.5, color="lightgreen"),
                stackgroup="one",
            )
        )
        fig.add_trace(
            go.Scatter(
                x=consumption_df["year"],
                y=consumption_df["other_renewable_consumption"],
                name="Other renewables Consumption",
                mode="lines",
                line=dict(width=0.5, color="darkgreen"),
                stackgroup="one",
            )
        )
        fig.add_trace(
            go.Scatter(
                x=consumption_df["year"],
                y=consumption_df["solar_consumption"],
                name="Solar Consumption",
                mode="lines",
                line=dict(width=0.5, color="yellow"),
                stackgroup="one",
            )
        )
        fig.add_trace(
            go.Scatter(
                x=consumption_df["year"],
                y=consumption_df["wind_consumption"],
                name="Wind Consumption",
                mode="lines",
                line=dict(width=0.5, color="darkblue"),
                stackgroup="one",
            )
        )
        fig.add_trace(
            go.Scatter(
                x=consumption_df["year"],
                y=consumption_df["gas_consumption"],
                name="Gas Consumption",
                mode="lines",
                line=dict(width=0.5, color="blue"),
                stackgroup="one",
            )
        )
        fig.add_trace(
            go.Scatter(
                x=consumption_df["year"],
                y=consumption_df["oil_consumption"],
                name="Oil Consumption",
                mode="lines",
                line=dict(width=0.5, color="darkred"),
                stackgroup="one",
            )
        )
        fig.add_trace(
            go.Scatter(
                x=consumption_df["year"],
                y=consumption_df["nuclear_consumption"],
                name="Nuclear Consumption",
                mode="lines",
                line=dict(width=0.5, color="purple"),
                stackgroup="one",
            )
        )
        fig.add_trace(
            go.Scatter(
                x=consumption_df["year"],
                y=consumption_df["hydro_consumption"],
                name="Hydro Consumption",
                mode="lines",
                line=dict(width=0.5, color="red"),
                stackgroup="one",
            )
        )
        fig.update_layout(
            title=f"Consumption for {country_chosen}",
            title_font_size=40,
            legend_font_size=20,
            width=1600,
            height=1400,
        )
        fig.update_xaxes(
            title_text="Year",
            title_font=dict(size=30, family="Verdana", color="black"),
            tickfont=dict(family="Calibri", color="darkred", size=25),
        )
        fig.update_yaxes(
            title_text="Different consumptions",  # range = (0,160),
            title_font=dict(size=30, family="Verdana", color="black"),
            tickfont=dict(family="Calibri", color="darkred", size=25),
        )
        return fig.show()

    def enrich_df(self) -> None:
        """
        Enriches the attribute "energy_df" with the column "emissions", which are the
        emissions per energy source in tonnes of CO2.
        
        Parameters
        ---------------
        emission_multis: dictionary
            Dictionary with the emission multiplicators
        
        """

        # create a dictionary with the emission multiples per energy source
        emission_multis = {
            "biofuel": 1450,
            "coal": 1000,
            "gas": 455,
            "hydro": 90,
            "nuclear": 5.5,
            "oil": 1200,
            "solar": 53,
            "wind": 14,
        }

        def calc_emissions(df_row: pd.Series) -> float:
            """
            This method calculates the total emissions for a certain row of the dataframe

            Parameters
            ---------------
            emissions: list
                List of emissions per energy type
            part_consumption: pd.Dataframe
                Dataframe with the consumption columns
            emission_source: number
                Calculation of the total emissions and coverts the number into tonnes CO2
            df_row: pd.Series
                The row for which the emissions should be calculated

            Returns
            ---------------
            sum_emissions: float
                The total sum of emissions per row of the dataframe.
            """

            # create an empty list in which the emissions per energy type are saved
            emissions = []
            # run through the dictonary, find the corresponding value and do the computation
            for e_name, e_mult in emission_multis.items():
                # get the corresponding consumption value of the row of the dataset
                part_consumption = df_row[e_name + "_consumption"]

                # calculates the total emissions and coverts the number into tonnes CO2
                emission_source = (e_mult / 1e6) * (1e9 * part_consumption)

                # append the calculation to the emissions list
                emissions.append(emission_source)

            # sum up the individual values to a total sum of emissions per energy source and
            # return the value
            sum_emissions = sum(emissions)
            return sum_emissions

        self.energy_df["emissions"] = self.energy_df.apply(calc_emissions, axis=1)

    def plot_totalconsumption_emission(self, countries) -> None:
        """
        The user has the possibility to input one country.
        If the received country is not in the unique_column_values() list the method should raise a TypeError.
        The outcome is a plot of a line chart with two lines.
        One of them shows the total emission and the other one the total consumption for the chosen country.

        Parameters
        ---------------
        countries_list: list
            calls the list from unique column values
        new_energy_df: pd.Dataframe
            The original dataframe reduced to the consumption, year and country
            with an additional column "Total Consumption" and "emissions.
        df_plot: pd.Dataframe
            New dataframe filtered on ontly the received country string.

        Returns
        ---------------
        fig.show(): None
            The return is a line plot which shows the total consumptions over time for the chosen country

        """
        countries_list = self.unique_column_values()
        
        # Personalize the dataframe with the necessary "_consumption" columns to compare the total
        new_energy_df = self.energy_df[self.consumption_cols].copy()
        new_energy_df["total_consumption"] = new_energy_df.apply(np.sum, axis=1)
        new_energy_df["year"] = self.energy_df.index  # add year to new datadrame
        new_energy_df["country"] = self.energy_df["country"]
        new_energy_df["emissions"] = self.energy_df["emissions"]
        
        if isinstance(countries, str):
            if countries not in countries_list:
                raise ValueError("This country is not in the dataframe")
                
            df_plot = new_energy_df[new_energy_df["country"] == countries]
            
            # Create figure with secondary y-axis
            fig = make_subplots(specs=[[{"secondary_y": True}]])
            
            color = px.colors.qualitative.Alphabet[1]
            fig.add_trace(
                go.Scatter(x=df_plot['year'], y=df_plot['total_consumption'], name=f"Consumption of {countries}", line=dict(color=color)),
                secondary_y=False)
            
            fig.add_trace(
                go.Scatter(x=df_plot['year'], y=df_plot['emissions'], name=f"Emissions of {countries}", line=dict(color=color, width=4, dash='dot')),
                secondary_y=True)

            fig.update_yaxes(title_text="Total Consumption", secondary_y=False)
            fig.update_yaxes(title_text="Emissions", secondary_y=True)
            
            # Add figure title
            fig.update_layout(
                title_text=f"Total Energy Consumption and Emissions for {countries} over the years"
            )

            fig.show()
        else:
            # Create figure with secondary y-axis
            fig = make_subplots(specs=[[{"secondary_y": True}]])
            for i, country in enumerate(countries, start=1):
                if country not in countries_list:
                    raise ValueError("This country is not in the dataframe")
                
                df_plot = new_energy_df[new_energy_df["country"] == country]

                color = px.colors.qualitative.Alphabet[i]
                fig.add_trace(
                    go.Scatter(x=df_plot['year'], y=df_plot['total_consumption'], name=f"Consumption of {country}", line=dict(color=color, width=4)),
                    secondary_y=False)

                fig.add_trace(
                    go.Scatter(x=df_plot['year'], y=df_plot['emissions'], name=f"Emissions of {country}", line=dict(color=color, width=4, dash='dot')),
                    secondary_y=True)

                fig.update_yaxes(title_text="Total Consumption", secondary_y=False)
                fig.update_yaxes(title_text="Emissions", secondary_y=True)

                # Add figure title
                fig.update_layout(
                title_text=f"Total Energy Consumption and Emissions for {countries} over the years"
                )
            fig.show()

    def country_gdp(self, countries) -> None:
        """
        The user has the chance to input one country as a string or multiple as a list.
        If the received country is not in the unique_column_values() list the method should raise a TypeError.
        The outcome is a line chart of the different GDP's for the chosen country/countries.
        It compares the GDP column of each country over the years.

        Parameters
        ---------------
        countries_list: list
            calls the list from unique column values
        chosen_df: dataframe
            The country_gdp_df reduced to the country chosen

        Returns
        ---------------
        fig.show(): None
            The plot shows the evolution of the gdp over time
        """

        countries_list = self.unique_column_values()

        if isinstance(countries, list):
            
            for country in countries:
                if country not in countries_list:
                    raise ValueError("This country is not in the dataframe")
            chosen_df = self.energy_df[self.energy_df["country"].isin(countries)].copy()
        else:
            if countries not in countries_list:
                raise ValueError("This country is not in the dataframe")
            chosen_df = self.energy_df[self.energy_df["country"] == countries].copy()

        fig = px.line(
            chosen_df,
            x=chosen_df.index,
            y="gdp",
            color="country",
            title=f"GDP per {countries} over the years",
            hover_name="country",
            labels={"year": "Year", "GDP": "GDP in USD"},
        )
        return fig.show()

    def gapminder(self, inputyear) -> None:
        """
        This method should receive an argument year (user's choice) of type int.
        If the received argument is not an int, the method should raise a TypeError.
        This method should plot a scatter plot where x is gdp, y is total energy consumption,
        and the area of each dot is population.

        Parameters
        ------------
        countries_list: list
            calls the list from unique column values
            
        user_df: dataframe
            dataframe energydf filtered on inputyear 

        input_energy_df: dataframe
            all plots are based on this dataframe which is filtered and NaNs have been cleaned
            
        Returns
        ---------
        fig.show(): None
            Returns a scatterplot similar in design to the gapminder plot of Hans Rosling.

        Example
        ---------
        https://www.gapminder.org/tools/#$chart-type=bubbles&url=v1
        """

        if type(inputyear) not in [int]:
            raise TypeError("Chosen year is not int.")
        # clean dataframe
        user_df = self.energy_df[self.energy_df["year_int"] == inputyear].copy()
        user_df['total_consumption'] = user_df[self.consumption_cols].sum(axis=1)
        input_energy_df_first = user_df.dropna(subset=["gdp", "total_consumption"])
        input_energy_df = input_energy_df_first[
            input_energy_df_first["country"] != "World"
        ].copy()
        input_energy_df["population"].fillna(0, inplace=True)

        # plot
        fig = px.scatter(
            input_energy_df,
            x="gdp",
            y="total_consumption",
            color="country",
            size="population",
            hover_name="country",
            log_x=True,
            log_y=True,
            size_max=60,
            labels={
                "gdp": "GDP in USD",
                "total_consumption": "Energy Consumption in Terrawat",
            },
            title=f"Worldwide Trend in GDP, Energy Consumption and Population per Country for the year {inputyear}",
        )
        return fig.show()

    def population_dev(self, countries) -> None:
        """
        The user has the possibility to input a country string or a list with multiple country strings
        If the received country is not in the unique_column_values() list the method should raise a TypeError.
        The outcome is a plot of a line chart of the population for the chosen country.
        This method receives a string or list with the countries.
        It compares the population of each country over the years.

        Parameters
        ---------------
        countries_list: list
            calls the list from unique column values
            
        chosen_df: dataframe
            The country_gdp_df reduced to the country chosen

        Returns
        ---------------
        fig.show(): None
            The plot shows the evolution of the gdp over time
        """
        countries_list = self.unique_column_values()

        if isinstance(countries, str):
            if countries not in countries_list:
                raise ValueError("This country is not in the dataframe")
            chosen_df = self.energy_df[self.energy_df["country"] == countries].copy()
        else:
            for country in countries:
                if country not in countries_list:
                    raise ValueError("This country is not in the dataframe")
            chosen_df = self.energy_df[self.energy_df["country"].isin(countries)].copy()
        fig = px.line(
            chosen_df,
            x=chosen_df.index,
            y="population",
            color="country",
            title="Population development over the years",
            hover_name="country",
            labels={"year": "Year", "population": "Population"},
        )
        return fig.show()

    def emission_scatter(self, inputyear) -> None:
        """
        This method should receive an argument year (user's choice) of type int.
        If the received argument is not an int, the method should raise a TypeError.
        Creates a scatter plot between emissions and consumption for all countries.
        The size of the dots is the population. The user has to determine a year.

        Parameter
        ---------------
        countries_list: list
            calls the list from unique column values
            
        inputyear: int
            The year for which the scatter plot should be created.
        
        filter1_df: pd.Dataframe
            filters the dataframe energy_df on year, cleans it from NaNs and adds total_consumption and removes the country World

        Returns
        ---------------
        fig.show(): None
            The scatter plot shows the energy consumption and the corresponding emissions
            of multiple countries for a particular year.
        """
        countries_list = self.unique_column_values()

        if type(inputyear) not in [int]:
            raise TypeError("Chosen year is not int.")

        # clean dataset
        filter1_df = self.energy_df[self.energy_df["year_int"] == inputyear].copy()
        filter1_df['total_consumption'] = filter1_df[self.consumption_cols].sum(axis=1)
        filter1_df["total_consumption"].fillna(0, inplace=True)
        filter1_df["emissions"].fillna(0, inplace=True)
        filter1_df["population"].fillna(0, inplace=True)
        filter1_df = filter1_df[filter1_df["country"] != "World"]
        # plot the data
        fig = px.scatter(
            filter1_df,
            x="emissions",
            y="total_consumption",
            color="country",
            size="population",
            hover_name="country",
            log_x=True,
            log_y=True,
            size_max=60,
            labels={
                "emissions": "Emissions in tC02eq/tWhSD",
                "total_consumption": "Energy Consumption in Terawat",
            },
            title="Worldwide Trend in Emissions, Energy Consumption, and Population per Country in "
            + str(inputyear),
        )
        return fig.show()

    def forecast_arima(self, country: str, periods: int) -> None:
        """
        This method should receive an argument periods (user's choice) of type int and a country of type string
        If the received year is not bigger 1 or the country not in the list of unique_column_values() 
        the method should raise a TypeError.
        The outcome are two  line chart plots which will show the past and predicted values for
        total consumption and also total emissions. The prediction is done by the arima model.
        It will predict the number of periods in the future, which the user inputed. 
        
        Parameters
        ---------------
        countries_list: list
            calls the list from unique column values
            
        data: dataframe
            The original dataframe reduced to the chosen year and consumption columns.
            Afterwards the total consumption is calculated
            
        finaldf: dataframe
            The original dataframe reduced to the chosen year and total consumption

        arima: model
            THe ARMIA model
            
        outcome: dataframe
            Dataframe with the arrays forecast_data and forecast_index

        Returns
        ---------------
        fig.show(): None
            The plot shows the past and predicted total consumption and emission
            for the chosen country over chosen timeperiod
        """

        countries_list = self.unique_column_values()

        if periods <= 1:
            raise ValueError(
                "The period timeframe has to be longer than 1 period"
            )
        if country not in countries_list:
            raise ValueError(
                "This country is not in the dataframe"
            )
        data = self.energy_df[self.energy_df["country"] == country].loc[
            :, self.consumption_cols
        ]
        data["total_consumption"] = data.apply(np.sum, axis=1)
        finaldf = data[["total_consumption"]]

        arima = ARIMA(finaldf.iloc[:-2], order=(3, 1, 0))
        result = arima.fit()

        outcome = result.predict(
            len(finaldf) - 2, len(finaldf) + periods + 1, typ="levels"
        )
         # -------------------------------------------------------
        # model emissions
        finaldf2 = self.energy_df[self.energy_df["country"] == country][
            "emissions"
        ].copy()

        arima2 = ARIMA(finaldf2.iloc[:-2], order=(3, 1, 0))
        result2 = arima2.fit()

        outcome2 = result2.predict(
            len(finaldf2) - 2, len(finaldf2) + periods + 1, typ="levels"
        )
        plotting_data2 = finaldf2[:-2]
        plotting_data2 = plotting_data2.to_frame()
        outcome2 = outcome2.to_frame()
        
        plotting_data = finaldf[:-2]
        outcome = outcome.to_frame()   
        # -------------------------------------------------------
        fig = make_subplots(rows=1, cols=2)

        fig.add_trace(
            go.Scatter(x=plotting_data.index, y=plotting_data.total_consumption, 
                       name=f"Total consumption for {country}"),
            row=1, col=1
        )
        
        fig.add_trace(
            go.Scatter(x=outcome.index, y=outcome.predicted_mean, 
                       name="Total consumption forecast"),
            row=1, col=1
        )
        fig.add_trace(
            go.Scatter(x=plotting_data2.index, y=plotting_data2["emissions"], name= "Total Emissions"),
            row=1, col=2
        )
        fig.add_trace(
            go.Scatter(x=outcome2.index, y=outcome2.predicted_mean, name= "Total Emissions forecast"),
            row=1, col=2
        )

        fig.update_layout(height=600, width=1400, title_text=f"Total Consumption and Toal Emissions forecast for {country}")
        fig.show()

