"""
This is method 5 which shows the GDP development over the years.
"""

import seaborn as sns
import plotly.express as px

def country_gdp(self, countries):
        """
        The outcome is a line chart of the different GDP's for the chosen country.
        This method receives a string or list with the countries.
        It compares the GDP column of each country over the years.

        Parameters
        ---------------
        country_gdp_df: dataframe
            The original dataframe reduced to the year, gdp and country columns
        chosen_df: dataframe
            The country_gdp_df reduced to the country chosen

        Returns
        ---------------
        my_output: lineplot
            The plot shows the evolution of the gdp over time
        """

        if isinstance(countries, str):
            chosen_df = self.energy_df[self.energy_df["country"] == countries].copy()
        else:
            chosen_df = self.energy_df[self.energy_df["country"].isin(countries)].copy()
        fig = px.line(chosen_df, x="year", y="gdp", color= "country", title='GDP per Country over the years',
                hover_name="country",
                         labels={
                     "year": "Year",
                     "GDP": "GDP in USD"
                 })
        return fig.show()
