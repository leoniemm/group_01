"""
This is method 02 which returns the unique countries.
"""

import pandas as pd

def unique_column_values(self) -> list: # Leonie had self in der Klammer
        """
        This function outputs a list of the unique countries in the dataframe

        Parameters
        ---------------
        unique_values: list
        list of the unique countries in the dataframe
        Returns
        ---------------
        my_output: list
        list of the unique countries in the dataframe
        """
        unique_values = list(self.energy_df["country"].unique())
        return unique_values
